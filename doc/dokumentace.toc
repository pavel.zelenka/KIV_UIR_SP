\select@language {czech}
\contentsline {section}{\numberline {1}Zad\IeC {\'a}n\IeC {\'\i }}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Zad\IeC {\'a}n\IeC {\'\i } probl\IeC {\'e}mu}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Form\IeC {\'a}t anotovan\IeC {\'e}ho souboru}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Anal\IeC {\'y}za probl\IeC {\'e}mu}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Algoritmy pro tvorbu p\IeC {\v r}\IeC {\'\i }znak\IeC {\r u}}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Algoritmy pro detekci}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Popis \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Algoritmus Bag-of-Words}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Algoritmus bigram}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Algoritmus TF-IDF}{6}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Algoritmus k-nejbli\IeC {\v z}\IeC {\v s}\IeC {\'\i }ch soused\IeC {\r u}}{7}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Algoritmus nejbli\IeC {\v z}\IeC {\v s}\IeC {\'\i }ho t\IeC {\v e}\IeC {\v z}i\IeC {\v s}t\IeC {\v e}}{7}{subsection.3.5}
\contentsline {section}{\numberline {4}U\IeC {\v z}ivatelsk\IeC {\'a} dokumentace}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Po\IeC {\v z}adavky}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } aplikace}{8}{subsection.4.2}
\contentsline {section}{\numberline {5}Z\IeC {\'a}v\IeC {\v e}r}{9}{section.5}
\contentsline {section}{\numberline {6}Reference}{9}{section.6}
