## KIV/UIR - Semestralni prace pro ak. rok 2017/18
### Automaticka detekce udalosti

Ve zvolenem programovacim jazyce navrhnete a implementujte program, ktery umozni
automaticky detekovat udalosti z kratkych textovych zprav. Pri reseni budou splneny
nasledujici podminky:

- Udalost definujeme podle Cambridge slovniku jako “anything that happens, especially something important and unusual”

- Pouzijte data ze soc. site Twitter v ceskem jazyce.

- Pro vyhodnoceni presnosti implementovanych algoritmu bude NUTNE tweety rucne oznackovat.

- Implementujte alespon tri ruzne algoritmy (z prednasek i vlastni) pro tvorbu priznaku reprezentujici textovou zpravu.

- Implementujte alespon dve ruzne metody detekce udalosti dle vlastni volby s vyuzitim metod reprezentace zprav viz vyse.

- Funkcnost programu bude nasledujici:

    - Spusteni s parametry: mnozina obsahujici udalosti, parametrizacni algoritmus, algoritmus detekce, (dalsi volitelne parametry)
    
    - Program provede detekci udalosti dle zadanych parametru, vysledky detekce ulozi do souboru a zaroven vyhodnoti uspesnost detekce.
    
- Ohodnotte kvalitu detekcniho algoritmu na anotovanych datech, pouzijte metriky presnost, uplnost a F-mira (precision, recall and F-measure). Otestujte vsechny konfigurace programu (tedy celkem 6 vysledku).
