package cz.pavelzelenka.tweetanalyzer.representation;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Vyctovy typ parametrizacnich algoritmu
 * @author Pavel Zelenka
 *
 */
public enum TextRepresentation {
	BOW("Bag of Words", BoW.class),
	N_GRAM("N-Gram", NGram.class),
	TF_IDF("TF-IDF", TFIDF.class);
	
	/** nazev */
    private final String name;

	/** reprezentace */
    private final Class representation;
    
    /**
     * Konstruktor
     * @param name nazev
     * @param representation reprezentace
     */
    private TextRepresentation(String name, Class representation) {
        this.name = name;
        this.representation = representation;
    }
    
    /**
     * Vrati nazev
     * @return nazev
     */
    public String getName() {
    	return this.name;
    }
	
    /**
     * Vrati instanci reprezentace textu
     * @return instance tridy reprezentace textu
     */
    public ITextRepresentation getInstance() {
    	if(representation != null) {
    		try {
    			Constructor<?>[] c = Class.forName(representation.getName()).getConstructors();
    			ITextRepresentation textRepresentation = (ITextRepresentation) c[0].newInstance();
    			return textRepresentation;
    		} catch (SecurityException | ClassNotFoundException | InstantiationException |
    				IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
    			e.printStackTrace();

    		}
    	}
    	return null;
    }
	
	@Override
	public String toString() {
		return this.name;
	}
	
	/**
	 * Vrati seznam moznych reprezentaci textu
	 * @return seznam moznych reprezentaci textu
	 */
	public static ObservableList<TextRepresentation> getDefaultList() {
		ObservableList<TextRepresentation> result = FXCollections.observableArrayList(TextRepresentation.values());
		return result;
	}
	
}
