package cz.pavelzelenka.tweetanalyzer.representation;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.pavelzelenka.tweetanalyzer.AppData;
import cz.pavelzelenka.tweetanalyzer.model.Tweet;
import javafx.util.Pair;

/**
 * TF-IDF
 * @author Pavel Zelenka
 */
public class TFIDF implements ITextRepresentation {

	/** Mapa - slovo, pocet, index */
	private static Map<String, Pair<Integer, Integer>> words = new LinkedHashMap<String, Pair<Integer, Integer>>();
	
	/**
	 * Vytvoreni seznamu slov
	 */
	public void createWordList() {
		// Nejcastejsi slovo
		//String maxs = "";
		//int maxi = 0;
		
		int index = 0;
		for(Tweet tweet : AppData.tweets) {
			for(String word : getWords(tweet)) {
				if(!words.containsKey(word)) {
					// Pridani slova
					int counts = 1;
					words.put(word, new Pair<Integer, Integer>(counts, index));
					index++;
				} else {
					// Upraveni zaznamu
					int counts = words.get(word).getKey() + 1;
					int usedIndex = words.get(word).getValue();
					words.put(word, new Pair<Integer, Integer>(counts, usedIndex));
					// Zmeni nejcastejsi slovo
					//if(counts > maxi) {
					//	maxi = counts;
					//	maxs = word;
					//}
				}
			}
		}
		// Vypise nejcastejsi slovo
		//System.out.println(maxs);
	}
	
	/**
	 * Vytvoreni vektoru
	 */
	public void createVectors() {
		for(Tweet tweet : AppData.tweets) {
			List<String> listOfWords = getWords(tweet);
			tweet.setVector(new double[words.size()]);
			for(int i = 0; i < listOfWords.size(); i++) {
				String word = listOfWords.get(i);
				if(words.containsKey(word)) {
					int index = words.get(word).getValue();
					// pocet vyskytu slova ve tweetu
					double tfCount = listOfWords.stream().filter(w -> w.equals(word)).count();
					// pocet_vyskytu_slova_ve_tweetu / pocet_slov_tweetu
					double tf = tfCount/listOfWords.size();
					// pocet tweetu
					double idfCount = AppData.tweets.size();
					// pocet_tweetu / pocet_vyskytu_slova
					double idf = Math.log((idfCount/(double)words.get(word).getKey()));
					// soucin tf * idf
					double value = tf * idf;
					tweet.getVector()[index] = value;
				}
			}
			// Vypise vektor
			//System.out.println(Arrays.toString(tweet.getVector()));
		}
	}
	
	/**
	 * Vrati slova tweetu
	 * @param tweet
	 * @return slova tweetu
	 */
	public List<String> getWords(Tweet tweet) {
		List<String> listOfWords = new ArrayList<>();
		String[] words = tweet.getText().split("\\s+");
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].toLowerCase();
			words[i] = Normalizer.normalize(words[i], Normalizer.Form.NFD);
			words[i] = words[i].replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
			words[i] = words[i].replaceAll("[^\\w]", "");
			words[i] = words[i].replaceAll("[\\d.]", "");
			words[i] = filteredWords(words[i]);
		}
		for (int i = 0; i < words.length; i++) {
			if(words[i] != null && !words[i].isEmpty()) {
				listOfWords.add(words[i]);
			}
		}
		return listOfWords;
	}
	
	/**
	 * Filtrovana slova
	 * @param word slovo
	 * @return slovo prosle filtrem
	 */
	public static String filteredWords(String word) {
		List<String> filteredWords = new ArrayList<>();
		filteredWords.addAll(Arrays.asList(
				"CT24zive", "echocz", "rt", "zpravy", "dnes", "byl", "by", "jen", "jsme", "bylo", "mu", "ma", "ho", "cr", "cz", "ale", "via", "uz", "i", "a", "tak",
				"kam", "jestli", "protoze", "zatim", "nebo", "dalsi", "prekvapilo", "konecne", "zacina", "bude", "nebude", "takto", "bez", "beze", "blizko",
				"dle", "do", "k", "ke", "kol", "krom", "krome", "ku", "kvuli", "mezi", "mimo", "na", "nad", "nade", "naproti", "si", "je",
				"navzdory", "nedaleko", "o", "ob", "od", "ode", "ohledne", "okolo", "oproti", "po", "pobliz", "pod", "pode", "podel", "podle", "podleva",
				"podliva", "pomoci", "pred", "prede", "pres", "prese", "pri", "pro", "prostrednictvim", "proti", "s", "se", "skrz", "skrze", "stran",
				"u", "uprostred", "v", "vcetne", "ve", "vedle", "versus", "vinou", "vne", "vo", "vod", "vstric", "vuci", "vukol", "vz", "vzdor",
				"vzhledem", "z", "za", "ze", "zkraje", "zpod", "zpoza", "an", "ana", "ano", "any", "buhvico", "buhvikdo", "ci", "cisi", "co", "cokoli",
				"cokoliv", "copak", "cos", "cosi", "coz", "coze", "ja", "jakkoli", "jakovy", "jaky", "jaka", "jake", "jaci", "jakykoli",
				"jakakoli", "jakekoli", "jacikoli", "jakykoliv", "jakakoliv", "jakekoliv", "jacikoliv", "jakysi", "jakasi", "jakesi", "jacisi",
				"jeho", "jehoz", "jejiz", "jeji", "jejich", "jejichz", "jejiz", "jenz",	"jiz", "jez", "jich", "kazdy", "kdekoli", "kdekoliv", "kdo",
				"kdokoli", "kdokoliv", "kdosi", "kdy", "kdykoli", "kdykoliv", "kterej", "ktery", "ktera", "ktere", "kteri", "kterykoliv", "ky", "lecco",
				"leckdo", "lecktery", "lecktera", "lecktere", "leckteri", "malokdo", "me", "muj", "moje", "my", "my", "mi", "naky", "naka", "nake", "naci",
				"nas", "nase", "nasi", "neci", "neco", "nejaky", "nejaka", "nejake", "nejaci", "nekdo", "nektery", "nektera", "nektere", "nekteri",
				"nesvuj", "nesva", "nesve",	"nesvi", "nic", "nici", "nikdo", "nizadny", "nizadna", "nizadne", "nizadni", "odkdy", "on", "ona",
				"onano", "onen", "oni", "ono", "ony", "pranic", "prazadny", "prazadna", "prazadne", "prazadni", "sa", "sam", "sama", "samo", "sami",
				"samy", "sama", "same", "sami", "se", "svuj", "sva", "sve", "svi", "svoje", "svoji", "ta", "tenhle", "tahle", "tohle", "tihle", "takovy",
				"takova", "takove", "takovi", "takovyhle", "takovahle", "takovehle", "takovihle", "takovyto", "takovato", "takoveto", "takovito",
				"taky", "taka", "take", "taci", "tato", "ten", "tenhle", "tento", "tentyz", "ti", "tito", "to", "tohle", "toto", "tuten", "tvuj", "ty",
				"tyto", "tyz", "vas", "vesker", "veskery", "von", "vse", "vsecek", "vsechen", "vsechno", "vseliky", "vy", "zadny"));
		if(filteredWords.contains(word)) {
			return "";
		}
		return word;
	} 
	
	/**
	 * Vrati slovo na pozici
	 * @param index pozadovany index
	 * @return slovo na pozici
	 */
	public String getWordAtIndex(int index) {
		for(String word : words.keySet()) {
			int wordIndex = words.get(word).getValue();
			if(wordIndex == index) return word;
		}
		throw new ArrayIndexOutOfBoundsException("Neplatny index " + index + ".");
	}
	
	/**
	 * Vrati vyskyt slova na pozici
	 * @param index pozadovany index
	 * @return vyskyt slova na pozici
	 */
	public int getWordFrequencyByIndex(int index) {
		for(String word : words.keySet()) {
			int wordIndex = words.get(word).getValue();
			if(wordIndex == index) return words.get(word).getKey();
		}
		throw new ArrayIndexOutOfBoundsException("Neplatny index " + index + ".");
	}
	
	/**
	 * Vypise slova, pocet vyskytu a index
	 */
	public void printWords() {
		for(String word : words.keySet()) {
			int count = words.get(word).getKey();
			int index = words.get(word).getValue();
			System.out.println(word + "; count=" + count + "; index=" + index);
		}
	}
	
	/**
	 * Vrati pocet slov
	 * @return pocet slov
	 */
	public int size() {
		return words.size();
	}
	
}
