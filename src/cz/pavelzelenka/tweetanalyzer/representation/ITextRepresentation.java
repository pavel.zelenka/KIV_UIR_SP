package cz.pavelzelenka.tweetanalyzer.representation;

/**
 * Rozhrani pro reprezentace textu
 * @author Pavel Zelenka
 */
public interface ITextRepresentation {

	/**
	 * Vytvoreni seznamu slov
	 */
	public void createWordList();
	
	/**
	 * Vytvoreni vektoru
	 */
	public void createVectors();
	
	/**
	 * Vypise slova, pocet vyskytu a index
	 */
	public void printWords();
	
	/**
	 * Vrati pocet slov
	 * @return pocet slov
	 */
	public int size();
}
