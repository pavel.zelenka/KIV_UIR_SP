package cz.pavelzelenka.tweetanalyzer;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import cz.pavelzelenka.tweetanalyzer.classifier.Classifier;
import cz.pavelzelenka.tweetanalyzer.classifier.IClassifier;
import cz.pavelzelenka.tweetanalyzer.representation.ITextRepresentation;
import cz.pavelzelenka.tweetanalyzer.representation.TextRepresentation;
import cz.pavelzelenka.tweetanalyzer.util.CSVLoader;
import cz.pavelzelenka.tweetanalyzer.util.Distance;
import cz.pavelzelenka.tweetanalyzer.util.StatisticsManager;

/**
 * Hlavni trida aplikace
 * @author Pavel Zelenka
 */
public class AppMain {
	
	/** vystupni soubor statistiky */
	private static File outputFile = new File("stats.txt");
	
	/** vstupni soubor tweetu */
	public static File inputFile;
	
	/** Parametrizacni algoritmus pro tvorbu priznaku reprezentujici textovou zpravu */
	public static ITextRepresentation textRepresentation;
	
	/** Detekcni algoritmus */
	public static IClassifier classifier;
	
	/** Metrika */
	public static Distance.Metric metric = null;
	
	/**
	 * Hlavni metoda pro spusteni aplikace
	 * @param args argumenty pri spusteni
	 */
	public static void main(String[] args) {
		// Zadany parametry pri spusteni
		if(args.length > 2) {
			String inputFileArg = args[0];
			String representArg = args[1];
			String detectionArg = args[2];
			setInputFile(inputFileArg);
			setTextRepresentation(representArg);
			setClassifier(detectionArg);
			if(args.length > 3) {
				String metricArg = args[3];
				setMetric(metricArg);
				
			}
		// Nezadany parametry pri spusteni
		} else {
			printUsage();
			//demo1();
			//demo2();
			handleClose();
		}
		// Spusteni
		new AppMain();
	}

	public static void demo1() {
		inputFile = new File("tweets.csv");
		textRepresentation = TextRepresentation.BOW.getInstance();
		classifier = Classifier.K_NN.getInstance();
		metric = Distance.Metric.Euclidean;
	}
	
	public static void demo2() {
		inputFile = new File("tweets.csv");
		textRepresentation = TextRepresentation.TF_IDF.getInstance();
		classifier = Classifier.NEAREST_CENTROID.getInstance();
		metric = Distance.Metric.Euclidean;
	}
	
	/**
	 * Vypise navod k pouziti
	 */
	public static void printUsage() {
		System.out.println("Parametry aplikace: [soubor_tweetu] [parametrizace] [detekce] [metrika]\n"
				+ "\t soubor_tweetu: textovy soubor \n"
				+ "\t parametrizace: bow / ngram / tfidf \n"
				+ "\t detekce: knn / ncc \n"
				+ "\t metrika: euc / eus / man / cos \n"
				+ "Priklad: soubor.txt bow knn \n");
	}
	
	/**
	 * Nastavi vstupni soubor
	 * @param inputFileArg parametr pro vstupni soubor
	 */
	public static void setInputFile(String inputFileArg) {
		if(Files.exists(Paths.get(inputFileArg))) {
			inputFile = new File(inputFileArg);
		} else {
			// Ukonci
			System.out.println("Neplatny soubor tweetu.");
			handleClose();
		}
	}
	
	/**
	 * Nastavi pouzity detekcni algoritmus
	 * @param detectionArg parametr pro detekcni algoritmus
	 */
	public static void setClassifier(String detectionArg) {
		switch(detectionArg) {
			case "knn": classifier = Classifier.K_NN.getInstance();
				break;
			case "ncc": classifier = Classifier.NEAREST_CENTROID.getInstance();
				break;
			default: 
				// Ukonci
				System.out.println("Neplatny algoritmus detekce.");
				handleClose();
		}
	}
	
	/**
	 * Nastavi pouzity metriky
	 * @param metricArg parametr pro metriku
	 */
	public static void setMetric(String metricArg) {
		switch(metricArg) {
			case "cos": metric = Distance.Metric.Cosine;
				break;
			case "man": metric = Distance.Metric.Manhattan;
				break;
			case "euc": metric = Distance.Metric.Euclidean;
				break;
			case "eus": metric = Distance.Metric.EuclideanSquared;
				break;
			default: 
				System.out.println("Neplatna metrika.");
		}
	}
	
	/**
	 * Nastavi pouzity parametrizacni algoritmus
	 * @param representArg parametr pro parametrizacni algoritmus
	 */
	public static void setTextRepresentation(String representArg) {
		switch(representArg) {
			case "bow": textRepresentation = TextRepresentation.BOW.getInstance();
				break;
			case "ngram": textRepresentation = TextRepresentation.N_GRAM.getInstance();
				break;
			case "tfidf": textRepresentation = TextRepresentation.TF_IDF.getInstance();
				break;
			default: 
				// Ukonci
				System.out.println("Neplatny parametrizacni algoritmus.");
				handleClose();
		}
	}
	
	/** Ukonci aplikaci */
	private static void handleClose() {
		System.exit(0);
	}
	
	/**
	 * Konstruktor
	 */
	public AppMain() {
		AppData.tweets = CSVLoader.loadFile(inputFile);
		
		textRepresentation.createWordList();
		textRepresentation.createVectors();
		//textRepresentation.printWords();
		//System.out.println(textRepresentation.size());
		
		if(metric != null) classifier.setMetric(metric);
		classifier.start(AppData.tweets);
		
		System.out.println("\n== TOPICS STATS ===");
		StatisticsManager.printTopicsStatistics();
		System.out.println("\n== OVERALL STATS ===");
		StatisticsManager.printOverallStatistics();
		System.out.println("\n== SUCCESS RATE ===");
		StatisticsManager.printSuccessRate();
		StatisticsManager.saveStats(outputFile);
	}
	

}
