package cz.pavelzelenka.tweetanalyzer.classifier;

import java.util.List;

import cz.pavelzelenka.tweetanalyzer.model.Tweet;
import cz.pavelzelenka.tweetanalyzer.util.Distance;

/**
 * Rozhrani pro klasifikatory
 * @author Pavel Zelenka
 */
public interface IClassifier {

	/**
	 * Spusteni algoritmu
	 * @param tweets tweety
	 */
	public void start(List<Tweet> tweets);
	
	/**
	 * Nastaveni metriky
	 * @param metric metrika
	 */
	public void setMetric(Distance.Metric metric);
	
}
