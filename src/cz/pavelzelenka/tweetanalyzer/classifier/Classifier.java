package cz.pavelzelenka.tweetanalyzer.classifier;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import cz.pavelzelenka.tweetanalyzer.classifier.knn.KNN;
import cz.pavelzelenka.tweetanalyzer.classifier.nearestcentroid.NearestCentroid;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Vyctovy typ parametrizacnich algoritmu
 * @author Pavel Zelenka
 */
public enum Classifier {
	K_NN("K-NN", KNN.class),
	NEAREST_CENTROID("Nearest Centroid", NearestCentroid.class);
	
	/** nazev */
    private final String name;

	/** reprezentace */
    private final Class classifier;
    
    /**
     * Konstruktor
     * @param name nazev
     * @param classifier klasifikator
     */
    private Classifier(String name, Class classifier) {
        this.name = name;
        this.classifier = classifier;
    }
    
    /**
     * Vrati nazev
     * @return nazev
     */
    public String getName() {
    	return this.name;
    }
	
    /**
     * Vrati instanci klasifikatoru
     * @return instance tridy klasifikatoru
     */
    public IClassifier getInstance() {
    	if(classifier != null) {
    		try {
    			Constructor<?>[] c = Class.forName(this.classifier.getName()).getConstructors();
    			IClassifier classifier = (IClassifier) c[0].newInstance();
    			return classifier;
    		} catch (SecurityException | ClassNotFoundException | InstantiationException |
    				IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
    			e.printStackTrace();

    		}
    	}
    	return null;
    }
	
	@Override
	public String toString() {
		return this.name;
	}
	
	/**
	 * Vrati seznam moznych klasifikatoru
	 * @return seznam moznych klasifikatoru
	 */
	public static ObservableList<Classifier> getDefaultList() {
		ObservableList<Classifier> result = FXCollections.observableArrayList(Classifier.values());
		return result;
	}
	
}
