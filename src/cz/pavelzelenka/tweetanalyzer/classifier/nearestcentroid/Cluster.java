package cz.pavelzelenka.tweetanalyzer.classifier.nearestcentroid;

import java.util.ArrayList;
import java.util.List;

import cz.pavelzelenka.tweetanalyzer.model.Tweet;

/**
 * Shluk
 * @author Pavel Zelenka
 */
public class Cluster {

	/** teziste */
	private double[] centroid;
	
	/** tweety nalezici do shluku */
	private List<Tweet> elements = new ArrayList<Tweet>();

	/**
	 * Vytvoreni instance shluku
	 */
	public Cluster(double[] centroid) {
		this.centroid = centroid;
	}
	
	/**
	 * Prepocita polohu teziste
	 */
    public void calculateCentroid() {
    	// Shluk nesmi byt prazdny (musi se urcit velikost vektoru a neni mozne delit nulou!)
    	if(elements.isEmpty()) {
            throw new IllegalArgumentException("Shluk neobsahuje zadne tweety!");
        }
        
        // Velikost vektoru
        int size  = elements.get(0).getVector().length;
        
        // Vypocet teziste
        for(Tweet tweet : elements) {
        	double[] vector = tweet.getVector();
            for (int j = 0; j < size; j++) {
                centroid[j] += vector[j];
            }
        }
        for(int i = 0; i < size; i++) {
            centroid[i] /= elements.size();
        }   
    }
	
	/**
	 * Vrati teziste
	 * @return teziste
	 */
	public double[] getCentroid() {
		return centroid;
	}

	/**
	 * Nastavi teziste
	 * @param centroid teziste
	 */
	public void setCentroid(double[] centroid) {
		this.centroid = centroid;
	}

	/**
	 * Vrati seznam tweetu
	 * @return seznam tweetu
	 */
	public List<Tweet> getElements() {
		return elements;
	}
	
}
