package cz.pavelzelenka.tweetanalyzer.classifier.nearestcentroid;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cz.pavelzelenka.tweetanalyzer.AppData;
import cz.pavelzelenka.tweetanalyzer.classifier.IClassifier;
import cz.pavelzelenka.tweetanalyzer.model.Topic;
import cz.pavelzelenka.tweetanalyzer.model.Tweet;
import cz.pavelzelenka.tweetanalyzer.util.Distance;
import cz.pavelzelenka.tweetanalyzer.util.Distance.Metric;
import cz.pavelzelenka.tweetanalyzer.util.StatisticsManager;

/**
 * Nejblizsi teziste
 * @author Pavel Zelenka
 */
public class NearestCentroid implements IClassifier {

	/** vychozi velikost trenovaci mnoziny */
	public static final int TRAINING_SET_COUNT = 250;
	
	/** vychozi metrika */
	public static final Distance.Metric METRIC = Distance.Metric.Euclidean;
	
	/** velikost trenovaci mnoziny */
	private int trainingCount = TRAINING_SET_COUNT;
	
	/** teziste */
	private LinkedHashMap<Topic, Cluster> clusters = new LinkedHashMap<>();
	
	/** metrika */
	private Distance.Metric metric = METRIC;
	
	/**
	 * Spusteni algoritmu
	 * @param tweets tweety
	 */
	public void start(List<Tweet> tweets) {
		// Upraveni velikosti trenovaci mnoziny, kdyz je treba
		if(tweets.size() < trainingCount) {
			trainingCount = tweets.size()/2;
		}
		
		// Pridani tweetu z trenovaci mnoziny do shluku
		for(int i = 0; i < trainingCount; i++) {
			Tweet trainingTweet = tweets.get(i);
			addToCluster(trainingTweet);
		}
		
		// Detekce tematu
		for(int i = trainingCount; i < AppData.tweets.size(); i++) {
			Tweet unknownTweet = tweets.get(i);
			// Nejblizsi tema
			Topic nearestTopic = getNearestTopic(unknownTweet);
			// Vyhodnotit
			StatisticsManager.evaluate(nearestTopic, unknownTweet.getTopic());
			StatisticsManager.outputContent.add(nearestTopic + " (" + unknownTweet.getTopic() + "): " + unknownTweet.getText());
		}
		
	}
	
	/**
	 * Vrati nejblizsi tema dle teziste shluku s nejmensi vzdalenosti
	 * @param tweet zprava
	 * @return nejblizsi tema
	 */
	public Topic getNearestTopic(Tweet tweet) {
		double[] vector = tweet.getVector();
		double nearestDistance = Double.MAX_VALUE;
		Topic nearestTopic = Topic.NONE;
		for (Map.Entry<Topic, Cluster> entry : clusters.entrySet()) {
			Cluster cluster = entry.getValue();
			Topic topic = entry.getKey();
			double[] centroid = cluster.getCentroid();
			// Vzdalenost
			double distance = Distance.countDistance(centroid, vector, metric);
			if(distance < nearestDistance) {
				nearestDistance = distance;
				nearestTopic = topic;
			}
		}
		return nearestTopic;
	}
	
	/**
	 * Prida tweet do shluku naleziciho tematu
	 * @param tweet zprava
	 */
	public void addToCluster(Tweet tweet) {
		Topic topic = tweet.getTopic();
		if(!clusters.containsKey(topic)) {
			clusters.put(topic, new Cluster(tweet.getVector()));
		}
		Cluster cluster = clusters.get(topic);
		cluster.getElements().add(tweet);
		// Urceni teziste
		cluster.calculateCentroid();
	}
	
	@Override
	public void setMetric(Metric metric) {
		this.metric = metric;
	}
	
}
