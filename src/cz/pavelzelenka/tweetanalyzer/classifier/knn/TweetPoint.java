package cz.pavelzelenka.tweetanalyzer.classifier.knn;

import java.util.Comparator;

import cz.pavelzelenka.tweetanalyzer.model.Topic;

/**
 * Bod
 * @author Pavel Zelenka
 */
public class TweetPoint implements Comparator<TweetPoint> {

	/** Vzdalenost */
	private double distance;
	/** Tema */
	private Topic topic;
	
	/** Instance bodu se vzdalenosti 0 a bez zvoleneho tematu*/
	public TweetPoint() {
		this.distance = 0;
		this.topic = Topic.NONE;
	}
	
	/**
	 * Instance bodu
	 * @param distance vzdalenost
	 * @param topic tema
	 */
	public TweetPoint(double distance, Topic topic) {
		this.distance = distance;
		this.topic = topic;
	}
	
	/**
	 * Vrati vzdalenost
	 * @return vzdalenost
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * Vrati tema
	 * @return tema
	 */
	public Topic getTopic() {
		return topic;
	}

	@Override
	public int compare(TweetPoint o1, TweetPoint o2) {
		if(o1.distance > o2.distance) {
			return 1;
		} else if(o1.distance == o2.distance) {
			return 0;
		} else {
			return -1;
		}
	}
	
	@Override
	public String toString() {
		return "[" + topic + " : " + String.format("%.2f",distance) + "]";
	}
	
}
