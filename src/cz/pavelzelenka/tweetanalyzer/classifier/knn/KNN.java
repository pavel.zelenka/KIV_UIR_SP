package cz.pavelzelenka.tweetanalyzer.classifier.knn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.pavelzelenka.tweetanalyzer.AppData;
import cz.pavelzelenka.tweetanalyzer.classifier.IClassifier;
import cz.pavelzelenka.tweetanalyzer.model.Topic;
import cz.pavelzelenka.tweetanalyzer.model.Tweet;
import cz.pavelzelenka.tweetanalyzer.util.Distance;
import cz.pavelzelenka.tweetanalyzer.util.Distance.Metric;
import cz.pavelzelenka.tweetanalyzer.util.StatisticsManager;

/**
 * K-NN
 * @author Pavel Zelenka
 */
public class KNN implements IClassifier {

	/** vychozi velikost trenovaci mnoziny */
	public static final int TRAINING_SET_COUNT = 250;
	
	/** vychozi pocet nejblizsich soudedu */
	public static final int CLOSEST_NEIGHBORS = 3;
	
	/** vychozi metrika */
	public static final Distance.Metric METRIC = Distance.Metric.Euclidean;
	
	/** velikost trenovaci mnoziny */
	private int trainingCount = TRAINING_SET_COUNT;
	
	/** pocet nejblizsich soudedu */
	private int neighborsCount = CLOSEST_NEIGHBORS;
	
	/** metrika */
	private Distance.Metric metric = METRIC;
	
	/**
	 * Spusteni algoritmu
	 * @param tweets tweety
	 */
	public void start(List<Tweet> tweets) {
		// Upraveni trenovaci mnoziny, kdyz je treba
		if(tweets.size() < trainingCount) {
			trainingCount = tweets.size()/2;
		}
		// spusteni algoritmu
		for(int i = trainingCount; i < AppData.tweets.size(); i++) {
			// vzdalenosti tweetu z trenovaci mnoziny
			List<TweetPoint> tweetPoints = new ArrayList<>();
			// nejblizsi temata z trenovaci mnoziny
			Map<Topic, Integer> nearestTopics = new HashMap<Topic, Integer>();
			// analyzovany tweet
			Tweet unknownTweet = tweets.get(i);
			
			// Vzdalenosti vuci trenovaci mnozine
			for(int j = 0; j < trainingCount; j++) {
				Tweet trainingTweet = tweets.get(j);
				double distance = countDistance(unknownTweet, trainingTweet);
				tweetPoints.add(new TweetPoint(distance, trainingTweet.getTopic()));
			}
			
			// Serazeni dle vzdalenosti
			Collections.sort(tweetPoints, new TweetPoint());
			
			// Nejblizsi sousedi
			Topic bestMatch = Topic.NONE;
			int bestMatchCount = 0;
			for(int k = 0; k < neighborsCount; k++) {
				if(tweetPoints.size() <= k) break;
				TweetPoint tp = tweetPoints.get(k);
				
				int count = 0; 
				if(nearestTopics.containsKey(tp.getTopic())) {
					count = nearestTopics.get(tp.getTopic()) + 1;
					nearestTopics.put(tp.getTopic(), count);
				} else {
					count = 1;
					nearestTopics.put(tp.getTopic(), count);
				}
				if(count > bestMatchCount) {
					bestMatchCount = count;
					bestMatch = tp.getTopic();
				}
			}

			// Vyhodnotit
			StatisticsManager.evaluate(bestMatch, unknownTweet.getTopic());
			StatisticsManager.outputContent.add(bestMatch + " (" + unknownTweet.getTopic() + "): " + unknownTweet.getText());
		}
	}
	
	/**
	 * Vypocte vzdalenost mezi 2 tweety
	 * @param a tweet
	 * @param b tweet
	 * @return vzdalenost
	 */
	private double countDistance(Tweet a, Tweet b) {
		return Distance.countDistance(a.getVector(), b.getVector(), metric);
	}

	@Override
	public void setMetric(Metric metric) {
		this.metric = metric;
	}
	
}
