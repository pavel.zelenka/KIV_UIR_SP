package cz.pavelzelenka.tweetanalyzer;

import java.util.ArrayList;
import java.util.List;

import cz.pavelzelenka.tweetanalyzer.model.Topic;
import cz.pavelzelenka.tweetanalyzer.model.Tweet;

/**
 * Data aplikace
 * @author Pavel Zelenka
 */
public class AppData {

	/** seznam temat */
	public static List<Topic> topics = new ArrayList<>(Topic.getDefaultList());
	
	/** Seznam tweetu */
	public static List<Tweet> tweets = new ArrayList<>();
	
	/** Knihovni trida */
	private AppData() {}
	
}
