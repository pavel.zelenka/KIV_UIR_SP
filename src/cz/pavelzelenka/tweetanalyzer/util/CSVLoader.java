package cz.pavelzelenka.tweetanalyzer.util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.pavelzelenka.tweetanalyzer.model.Topic;
import cz.pavelzelenka.tweetanalyzer.model.Tweet;

/**
 * Nacitani CSV souboru
 * @author Pavel Zelenka
 */
public class CSVLoader {
	
	/** kodovani */
	private static String utf8 = "UTF8";
	
	/** oddelovac csv souboru */
	private static String split = ";";	
	
	/**
	 * Nacteni souboru
	 * @param file soubor
	 * @return seznam tweetu
	 */
	public static List<Tweet> loadFile(File file) {
		List<Tweet> tweets = new ArrayList<>();
		BufferedReader reader = null;
		int lineNumber = 0;				// pocet radek
		String line = "";
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), utf8));
			while ((line = reader.readLine()) != null) {
				lineNumber++;				
				Tweet tweet = new Tweet();			
				String[] column = line.split(split);

				// Event
				try {
					String value = column[0].replaceAll("\"", "");
					byte event = Byte.parseByte(value);
					if(event == 0) {
						tweet.setEvent(false);
					} else {
						tweet.setEvent(true);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				// Topic
				try {
					String value = column[1].replaceAll("\"", "");
					tweet.setTopic(Topic.getTopicByCode(value));
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				// ID
				try {
					String value = column[2].replaceAll("\"", "");
					long id = Long.parseLong(value);
					tweet.setId(id);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
				// text
				try {
					String value = column[5];
					value = value.replaceAll("_", " ");
					value = value.replaceAll("#", "");
					value = value.replaceAll("@", "");
					tweet.setText(value);
				} catch(Exception e) {
					e.printStackTrace();
				}
				tweets.add(tweet);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return tweets;
	}
	
}
