package cz.pavelzelenka.tweetanalyzer.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.pavelzelenka.tweetanalyzer.AppData;
import cz.pavelzelenka.tweetanalyzer.model.Statistics;
import cz.pavelzelenka.tweetanalyzer.model.Topic;

/**
 * Spravce statistik
 * @author Pavel Zelenka
 */
public class StatisticsManager {
	
	/** obsah pro vystup */
	public static List<String> outputContent = new ArrayList<>();
	
	/** seznam temat */
	public static List<Topic> topics = AppData.topics;
	
	/** statistiky */
	private static Map<Topic, Statistics> statistics = new HashMap<Topic, Statistics>();
	
	/** dobre urceno */
	public static int okay = 0;
	
	/** spatne urceno */
	public static int wrong = 0;
	
	/**
	 * Vrati statistiku tematu
	 * @param topic tema
	 * @return statistika tematu
	 */
	public static Statistics getStatistics(Topic topic) {
		if(!statistics.containsKey(topic)) {
			statistics.put(topic, new Statistics(topic.name()));
		}
		return statistics.get(topic);
	}
	
	/**
	 * Vyhodnotit
	 * @param expected ocekavany
	 * @param real skutecny
	 */
	public static void evaluate(Topic expected, Topic real) {
		if(expected.equals(real)) {
			okay++;
			for(Topic tn : topics) {
				if(!tn.equals(expected)) {
					getStatistics(tn).addTN();
				}
			}
			getStatistics(expected).addTP();
		} else {
			wrong++;
			getStatistics(expected).addFP();
			getStatistics(real).addFN();
		}
	}

	/**
	 * Vrati uspesnost
	 * @return uspesnost
	 */
	public static String getSuccessRate() {
		int total = okay + wrong;
		double success = (100D/(double)total) * (double)okay;
		String text = "okay=" + okay + "; wrong=" + wrong + "; success rate=" + String.format("%.2f",success) + "%";
		return text;
	}
	
	/**
	 * Vypise uspesnost
	 */
	public static void printSuccessRate() {
		String text = getSuccessRate();
		System.out.println(text);
	}
	
	/**
	 * Vrati statistiky temat
	 * @return statistiky temat
	 */
	public static List<String> getTopicsStatistics() {
		List<String> list = new ArrayList<>();
		for(Topic topic : topics) {
			Statistics stats = getStatistics(topic);
			String precision = String.format("%.2f",stats.precision()*100);
			String recall = String.format("%.2f",stats.recall()*100);
			String fMeasre = String.format("%.2f",stats.fMeasre()*100);
			String err = String.format("%.2f",stats.error()*100);
			String acc = String.format("%.2f",stats.overallAccuracy()*100);
			String text = stats.getName() + ": presnost=" + precision + 
					"%; uplnost=" + recall + "%; f-mira=" + fMeasre +
					"%; err=" + err + "%; acc=" + acc + "%";
			list.add(text);
		}
		return list;
	}
	
	/**
	 * Vypise statistiky temat
	 */
	public static void printTopicsStatistics() {
		List<String> list = getTopicsStatistics();
		for(String text : list) {
			System.out.println(text);
		}
	}
	
	/**
	 * Vrati celkove statistiky
	 * @return celkove statistiky
	 */
	public static String getOverallStatistics() {
		double[] overall = overallStatistics();
		String text = "presnost=" + String.format("%.2f",overall[0]*100) + "%" +
				"; uplnost=" + String.format("%.2f",overall[1]*100) + "%" +
				"; f-mira=" + String.format("%.2f",overall[2]*100) + "%" +
				"; err=" + String.format("%.2f",overall[3]*100) + "%" +
				"; acc=" + String.format("%.2f",overall[4]*100) + "%";
		return text;
	}
	
	/**
	 * Vypise celkove statistiky
	 */
	public static void printOverallStatistics() {
		String text = getOverallStatistics();
		System.out.println(text);
	}
	
	/**
	 * Celkova statistika
	 * @return statistika
	 */
	public static double[] overallStatistics() {
		// Pocet vsech vyhodnocovanych tweetu
		double totalTweets = okay + wrong;
		// Celkove statistiky
		double[] overall = new double[5];
		// Zapocitavat sloupec (nechci delit nulou...)
		double[] ok = new double[5];
		// Pruchod temat
		for(int i = 0; i < topics.size(); i++) {
			// Priprava objektu
			Topic topic = topics.get(i);
			Statistics stats = getStatistics(topic);
			// Pomer tematu na celkovym poctu teetu
			double topicTweets = stats.getFN() + stats.getTP();
			double ratio = topicTweets/totalTweets;
			// A statistika :)
			Double precision = stats.precision();
			if(!precision.isNaN()) {
				ok[0] += 1D * ratio;
				overall[0] += precision*ratio;
			}
			Double recall = stats.recall();
			if(!recall.isNaN()) {
				ok[1] += 1D * ratio;
				overall[1] += recall * ratio;
			}
			Double fMeasre = stats.fMeasre();
			if(!fMeasre.isNaN()) {
				ok[2] += 1D * ratio;
				overall[2] += fMeasre * ratio;
			}
			Double error = stats.error();
			if(!error.isNaN()) {
				ok[3] += 1D * ratio;
				overall[3] += error * ratio;
			}
			Double overallAccuracy = stats.overallAccuracy();
			if(!overallAccuracy.isNaN()) {
				ok[4] += 1D * ratio;
				overall[4] += overallAccuracy * ratio;
			}
		}
		for(int i = 0; i < 5; i++) {
			overall[i] /= ok[i];
		}
		return overall;
	}
	
	/** 
	 * Ulozi obsah do souboru
	 * @param outputFile vystupni soubor
	 */
	public static void saveStats(File outputFile) {
		List<String> content = new ArrayList<>();
		content.add("== TWEETS ===");
		content.addAll(outputContent);
		content.add("== TOPICS STATS ===");
		content.addAll(getTopicsStatistics());
		content.add("== OVERALL STATS ===");
		content.add(getOverallStatistics());
		content.add("== SUCCESS RATE ===");
		content.add(getSuccessRate());
		try {
			Files.deleteIfExists(outputFile.toPath());
			Files.write(outputFile.toPath(), content, StandardOpenOption.CREATE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
