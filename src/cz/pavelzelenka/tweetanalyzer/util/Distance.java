package cz.pavelzelenka.tweetanalyzer.util;

/**
 * Vypocet vzdalenosti
 * @author Pavel Zelenka
 */
public class Distance {
	
	/** Metriky */
	public enum Metric {
		Euclidean,
		EuclideanSquared,
		Manhattan,
		Cosine;
	}
	
	/**
	 * Vypocte vzdalenost mezi 2 vektory
	 * @param vectorA prvni vektor
	 * @param vectorB druhy vektor
	 * @param metric metrika
	 * @return vzdalenost
	 */
	public static double countDistance(double[] vectorA, double[] vectorB, Metric metric) {
		double distance = 0.0;
		switch(metric) {
			case Cosine:	distance = cosineSimilarity(vectorA, vectorB);
							break;
			case Manhattan:	distance = manhattanDistance(vectorA, vectorB);
							break;
			case EuclideanSquared:	distance = euclideanSquaredDistance(vectorA, vectorB);
							break;
			case Euclidean:
			default:		distance = euclideanDistance(vectorA, vectorB);
		}
		return distance;
	}
	
	/**
	 * Vypocte Euklidovskou vzdalenost mezi 2 vektory
	 * @param vectorA prvni vektor
	 * @param vectorB druhy vektor
	 * @return vzdalenost
	 */
	private static double euclideanDistance(double[] vectorA, double[] vectorB) {
		double distance = 0.0;
		for (int i = 0; i < vectorA.length; i++) {
			double d = vectorA[i] - vectorB[i];
			distance += d * d;
		}
		return Math.sqrt(distance);
	}
	
	/**
	 * Vypocte Euklidovskou vzdalenost mezi 2 vektory
	 * @param vectorA prvni vektor
	 * @param vectorB druhy vektor
	 * @return vzdalenost
	 */
	public static double euclideanSquaredDistance(double[] vectorA, double[] vectorB) {
        double distance = 0.0;
        double temp;
        for (int i = 0; i < vectorA.length; i++) {
            temp = vectorA[i] - vectorB[i];
            temp *= temp;
            distance += temp;
        }

        return distance;
    }
	
	/**
	 * Vypocte Manhattanskou vzdalenost mezi 2 vektory
	 * @param vectorA prvni vektor
	 * @param vectorB druhy vektor
	 * @return vzdalenost
	 */
	private static double manhattanDistance(double[] vectorA, double[] vectorB) {
		double distance = 0.0;
		for (int i = 0; i < vectorA.length; i++) {
			double d = Math.abs(vectorA[i] - vectorB[i]);
			distance += d;
		}
		return distance;
	}
	
	/**
	 * Vypocte Kosinovu vzdalenost mezi 2 vektory
	 * @param vectorA prvni vektor
	 * @param vectorB druhy vektor
	 * @return vzdalenost
	 */
	private static double cosineSimilarity(double[] vectorA, double[] vectorB) {
	    double dotProduct = 0.0;
	    double normA = 0.0;
	    double normB = 0.0;
	    for (int i = 0; i < vectorA.length; i++) {
	        dotProduct += vectorA[i] * vectorB[i];
	        normA += Math.pow(vectorA[i], 2);
	        normB += Math.pow(vectorB[i], 2);
	    }   
	    return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
	
}
