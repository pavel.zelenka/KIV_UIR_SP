package cz.pavelzelenka.tweetanalyzer.model;

public class Statistics {

	/** nazev */
	private String name;
	
	/** true positive */
	private double tp = 0;
	
	/** false positive */
	private double fp = 0;
	
	/** true negative */
	private double tn = 0;
	
	/** false negative */
	private double fn = 0;
	
	/**
	 * Instance statistiky
	 */
	public Statistics() {
		this("");
	}
	
	/**
	 * Instance statistiky
	 * @param name nazev statistiky
	 */
	public Statistics(String name) {
		this.name = name;
	}
	
	/**
	 * Vypocte celkovou spravnost
	 * @return celkova spravnost
	 */
	public double overallAccuracy() {
		return (tp + tn) / (tp + tn + fp + fn);
	}
	
	/**
	 * Vypocte celkovou chybu
	 * @return celkova chyba
	 */
	public double error() {
		return (fp + fn) / (tp + tn + fp + fn);
	}
	
	/**
	 * Vypocte presnost
	 * @return presnost
	 */
	public double precision() {
		return (tp) / (tp + fp);
	}
	
	/**
	 * Vypocte uplnost
	 * @return uplnost
	 */
	public double recall() {
		return (tp) / (tp + fn);
	}
	
	/**
	 * Vypocte f-miru
	 * @return f-mira
	 */
	public double fMeasre() {
		double precision = precision();
		double recall = recall();
		return (2 * precision * recall) / (precision + recall);
	}

	public void addTP() {
		this.tp += 1;
	}
	
	public void addFP() {
		this.fp += 1;
	}
	
	public void addTN() {
		this.tn += 1;
	}
	
	public void addFN() {
		this.fn += 1;
	}
	
	public double getTP() {
		return this.tp;
	}
	
	public double getFN() {
		return this.fn;
	}
	
	/**
	 * Vrati nazev statistiky
	 * @return nazev statistiky
	 */
	public String getName() {
		return name;
	}

	/**
	 * Nastavi nazev statistiky
	 * @param name nazev statistiky
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
