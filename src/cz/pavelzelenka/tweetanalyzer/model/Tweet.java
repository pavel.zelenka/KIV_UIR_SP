package cz.pavelzelenka.tweetanalyzer.model;

/**
 * Tweet
 * @author Pavel Zelenka
 */
public class Tweet {

	/** ID tweetu */
	private long id;
	
	/** Jedna se o udalost */
	private boolean event;
	
	/** Tema prispevku */
	private Topic topic;
	
	/** Text prispevku */
	private String text;
	
	/** Vektor */
	private double[] vector;
	
	/**
	 * Vytvoreni instance tweetu
	 */
	public Tweet() {
	}

	/**
	 * Vrati ID tweetu 
	 * @return ID tweetu 
	 */
	public long getId() {
		return id;
	}

	/**
	 * Nastavi ID tweetu 
	 * @param id ID tweetu 
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Jedna se o udalost
	 * @return udalost vrati true
	 */
	public boolean isEvent() {
		return event;
	}

	/**
	 * Nastavi, zdali se jedna o udalost 
	 * @param event udalost je true
	 */
	public void setEvent(boolean event) {
		this.event = event;
	}

	/**
	 * Vrati tema
	 * @return tema
	 */
	public Topic getTopic() {
		return topic;
	}

	/**
	 * Nastavi tema
	 * @param topic tema
	 */
	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	/**
	 * Vrati text
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Nastavi text
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Vrati vektor
	 * @return vektor
	 */
	public double[] getVector() {
		return vector;
	}

	/**
	 * Nastai vektor
	 * @param vector vektor
	 */
	public void setVector(double[] vector) {
		this.vector = vector;
	}
	
}
