package cz.pavelzelenka.tweetanalyzer.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Temata
 * @author Pavel Zelenka
 */
public enum Topic {
    POLITICS("po"),
    INDUSTRY("pr"),
    AGRICULTURE("ze"),
    SPORT("sp"),
    CULTURE("ku"),
    CRIME("kr"),
    WEATHER("pc"),
    OTHER("ji"),
    NONE("-");
	
	public String code;
	
	private Topic(String code) {
		this.code = code;
	}
	
	/**
	 * Vrati tema dle kodu, neodpovida-li zadne tema, nastavi NONE
	 * @param code kod
	 * @return tema
	 */
	public static Topic getTopicByCode(String code) {
		if(code == null) {
			throw new NullPointerException("Nezadan kod tematu.");
		}
		for(Topic topic : Topic.values()) {
			if(topic.code.equals(code)) {
				return topic;
			}
		}
		throw new IllegalArgumentException("Neplatny kod tematu \"" + code + "\".");
	}
	
	/**
	 * Vrati seznam vychozich temat
	 * @return seznam vychozich temat
	 */
	public static ObservableList<Topic> getDefaultList() {
		ObservableList<Topic> result = FXCollections.observableArrayList(Topic.values());
		return result;
	}
	
	/**
	 * Vrati pocet vychozich temat
	 * @return pocet vychozich temat
	 */
	public static int size() {
		return Topic.values().length;
	}
}
